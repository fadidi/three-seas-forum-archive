ThreeSeasForum::Application.routes.draw do
  resources :users, :only => [:index, :show]
  resources :posts, :only => [:index, :show]
  resources :topics, :only => [:index, :show]
  resources :forums, :only => [:index, :show]
  resources :search, :only => :index

  match '/faq', :to => 'statics#faq', :as => :faq

  root :to => 'forums#index'
end
